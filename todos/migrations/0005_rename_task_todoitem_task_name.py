# Generated by Django 5.0 on 2023-12-13 01:22

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0004_alter_todoitem_due_date"),
    ]

    operations = [
        migrations.RenameField(
            model_name="todoitem",
            old_name="task",
            new_name="task_name",
        ),
    ]
