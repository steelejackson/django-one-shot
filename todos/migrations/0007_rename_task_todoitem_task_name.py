# Generated by Django 5.0 on 2023-12-13 18:58

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0006_rename_task_name_todoitem_task"),
    ]

    operations = [
        migrations.RenameField(
            model_name="todoitem",
            old_name="task",
            new_name="task_name",
        ),
    ]
