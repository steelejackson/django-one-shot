from django.shortcuts import render, get_object_or_404, redirect

from todos.models import TodoList, TodoItem

from todos.forms import TodoForm, ItemForm



# Create your views here.

def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        'todo_list_list': lists,
    }

    

    return render(request, 'lists/all_lists.html', context)

#Create a view that shows the details of a particular to-do list, including its tasks

def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    tasks = TodoItem.objects.filter(list=todo)
    context = {
        'todo_object': todo,
        'tasks': tasks,
    }

    

    return render(request, 'lists/todo_list_detail.html', context)

def todo_list_create(request):
    if request.method == 'POST':
        form = TodoForm(request.POST)
        if form.is_valid():
            new_list = form.save()
            return redirect('todo_list_detail', id=new_list.id)
    else:
        form = TodoList()
    context = {
        'form': form,
    }

    return render(request, 'lists/todo_list_create.html', context)

def todo_list_update(request, id):
    edited_list = get_object_or_404(TodoList, id=id)

    if request.method == 'POST':
        form = TodoForm(request.POST, instance=edited_list)
        if form.is_valid():
            edited_list = form.save()
            return redirect('todo_list_detail', id=edited_list.id)
    else:
        form = TodoForm(instance=edited_list)

    context = {
        'form': form,
        'edited_list': edited_list,
    }

    return render(request, 'lists/todo_list_update.html', context)

def todo_list_delete(request, id):
    list_to_delete = TodoList.objects.get(id=id)
    if request.method == 'POST':
        list_to_delete.delete()
        return redirect('todo_list_list')
    return render(request, 'lists/todo_list_delete.html')

def todo_item_create(request):
    
    if request.method == 'POST':
        form = ItemForm(request.POST)
        #new_item = ItemForm(request.POST)
        if form.is_valid():
            new_item = form.save()
            return redirect('todo_list_detail', id=new_item.list.id)
    else:
        form = ItemForm()
        
    context = {
        'form': form,
    }

    return render(request, 'lists/todo_item_create.html', context)

def todo_item_update(request, id):
    item_to_edit = TodoItem.objects.get(id=id)

    if request.method == 'POST':
        form = ItemForm(request.POST, instance=item_to_edit)
        if form.is_valid():
            item_to_edit = form.save()
            return redirect('todo_list_list')
    else:
        form = ItemForm(instance=item_to_edit)

    context = {
        'form': form,
        'item_to_edit': item_to_edit,
    }

    return render(request, 'lists/todo_item_update.html', context)


